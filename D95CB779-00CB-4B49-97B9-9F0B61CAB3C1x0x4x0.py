# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.01
# By python version 3.5.2 (v3.5.2:4def2a2901a5, Jun 25 2016, 22:01:18) [MSC v.1900 32 bit (Intel)]
# From type library 'biokey.ocx'
# On Mon Jul 25 16:16:27 2016
'ZKSoftware ZKFinger Engine 4.x'
makepy_version = '0.5.01'
python_version = 0x30502f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{D95CB779-00CB-4B49-97B9-9F0B61CAB3C1}')
MajorVersion = 4
MinorVersion = 0
LibraryFlags = 10
LCID = 0x0

from win32com.client import DispatchBaseClass
class IZKFPEngX(DispatchBaseClass):
	'Dispatch interface for ZKFPEngX Control'
	CLSID = IID('{161A8D2D-3DDE-4744-BA38-08F900D10D6D}')
	coclass_clsid = IID('{CA69969C-2F27-41D3-954D-A48B941C3BA7}')

	def AddBitmap(self, BitmapHandle=defaultNamedNotOptArg, ValidRectX1=defaultNamedNotOptArg, ValidRectY1=defaultNamedNotOptArg, ValidRectX2=defaultNamedNotOptArg
			, ValidRectY2=defaultNamedNotOptArg, DPI=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(201, LCID, 1, (11, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),BitmapHandle
			, ValidRectX1, ValidRectY1, ValidRectX2, ValidRectY2, DPI
			)

	def AddImageFile(self, AFileName=defaultNamedNotOptArg, ADPI=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(63, LCID, 1, (11, 0), ((8, 1), (3, 1)),AFileName
			, ADPI)

	def AddRegTemplateFileToFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, pRegTemplateFile=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(20, LCID, 1, (3, 0), ((3, 1), (3, 1), (8, 1)),fpcHandle
			, FPID, pRegTemplateFile)

	def AddRegTemplateFileToFPCacheDBEx(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, pRegTemplateFile=defaultNamedNotOptArg, pRegTemplate10File=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(216, LCID, 1, (3, 0), ((3, 1), (3, 1), (8, 1), (8, 1)),fpcHandle
			, FPID, pRegTemplateFile, pRegTemplate10File)

	def AddRegTemplateStrToFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, ARegTemplateStr=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(35, LCID, 1, (3, 0), ((3, 1), (3, 1), (8, 1)),fpcHandle
			, FPID, ARegTemplateStr)

	def AddRegTemplateStrToFPCacheDBEx(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, ARegTemplateStr=defaultNamedNotOptArg, ARegTemplate10Str=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(218, LCID, 1, (3, 0), ((3, 1), (3, 1), (8, 1), (8, 1)),fpcHandle
			, FPID, ARegTemplateStr, ARegTemplate10Str)

	def AddRegTemplateToFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, pRegTemplate=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(15, LCID, 1, (3, 0), ((3, 1), (3, 1), (12, 1)),fpcHandle
			, FPID, pRegTemplate)

	def AddRegTemplateToFPCacheDBEx(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg, pRegTemplate=defaultNamedNotOptArg, pRegTemplate10=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(214, LCID, 1, (3, 0), ((3, 1), (3, 1), (12, 1), (12, 1)),fpcHandle
			, FPID, pRegTemplate, pRegTemplate10)

	def BeginCapture(self):
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), (),)

	def BeginEnroll(self):
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def CancelCapture(self):
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	def CancelEnroll(self):
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def CompressTemplate(self, ATemplate=defaultNamedNotOptArg):
		return self._ApplyTypes_(68, 1, (12, 0), ((8, 1),), 'CompressTemplate', None,ATemplate
			)

	def ControlSensor(self, ACode=defaultNamedNotOptArg, AValue=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(209, LCID, 1, (3, 0), ((3, 1), (3, 1)),ACode
			, AValue)

	def ConvertAttTemplate(self, ATemplate=defaultNamedNotOptArg):
		return self._ApplyTypes_(70, 1, (12, 0), ((12, 1),), 'ConvertAttTemplate', None,ATemplate
			)

	def ConvertToBiokey(self, OriTemplate=defaultNamedNotOptArg):
		return self._ApplyTypes_(204, 1, (12, 0), ((12, 1),), 'ConvertToBiokey', None,OriTemplate
			)

	def CreateFPCacheDB(self):
		return self._oleobj_.InvokeTypes(9, LCID, 1, (3, 0), (),)

	def CreateFPCacheDBEx(self):
		return self._oleobj_.InvokeTypes(212, LCID, 1, (3, 0), (),)

	def DecodeTemplate(self, ASour=defaultNamedNotOptArg, ADest=defaultNamedNotOptArg):
		return self._ApplyTypes_(2, 1, (11, 0), ((8, 1), (16396, 3)), 'DecodeTemplate', None,ASour
			, ADest)

	def DecodeTemplate1(self, ASour=defaultNamedNotOptArg):
		return self._ApplyTypes_(31, 1, (12, 0), ((8, 1),), 'DecodeTemplate1', None,ASour
			)

	def DongleIsExist(self):
		return self._oleobj_.InvokeTypes(23, LCID, 1, (11, 0), (),)

	def DongleMemRead(self, p1=defaultNamedNotOptArg, p2=defaultNamedNotOptArg, buf=defaultNamedNotOptArg):
		return self._ApplyTypes_(30, 1, (11, 0), ((16387, 3), (16387, 3), (16396, 3)), 'DongleMemRead', None,p1
			, p2, buf)

	def DongleMemWrite(self, p1=defaultNamedNotOptArg, p2=defaultNamedNotOptArg, buf=defaultNamedNotOptArg):
		return self._ApplyTypes_(38, 1, (11, 0), ((16387, 3), (16387, 3), (16396, 3)), 'DongleMemWrite', None,p1
			, p2, buf)

	def DongleSeed(self, lp2=defaultNamedNotOptArg, p1=defaultNamedNotOptArg, p2=defaultNamedNotOptArg, p3=defaultNamedNotOptArg
			, p4=defaultNamedNotOptArg):
		return self._ApplyTypes_(29, 1, (11, 0), ((16387, 3), (16387, 3), (16387, 3), (16387, 3), (16387, 3)), 'DongleSeed', None,lp2
			, p1, p2, p3, p4)

	def DongleUserID(self):
		return self._oleobj_.InvokeTypes(28, LCID, 1, (3, 0), (),)

	def EncodeTemplate(self, ASour=defaultNamedNotOptArg, ADest=defaultNamedNotOptArg):
		return self._ApplyTypes_(3, 1, (11, 0), ((12, 1), (16392, 3)), 'EncodeTemplate', None,ASour
			, ADest)

	def EncodeTemplate1(self, ASour=defaultNamedNotOptArg):
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(33, LCID, 1, (8, 0), ((12, 1),),ASour
			)

	def EndEngine(self):
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), (),)

	def ExtractImageFromTerminal(self, AOriImage=defaultNamedNotOptArg, Size=defaultNamedNotOptArg, AAutoIdentify=defaultNamedNotOptArg, iResult=defaultNamedNotOptArg):
		return self._ApplyTypes_(211, 1, (3, 0), ((12, 1), (3, 1), (11, 1), (16396, 3)), 'ExtractImageFromTerminal', None,AOriImage
			, Size, AAutoIdentify, iResult)

	def ExtractImageFromURU(self, AOriImageStr=defaultNamedNotOptArg, Size=defaultNamedNotOptArg, AAutoIdentify=defaultNamedNotOptArg, iResult=defaultNamedNotOptArg):
		return self._ApplyTypes_(691, 1, (3, 0), ((8, 1), (3, 1), (11, 1), (16396, 3)), 'ExtractImageFromURU', None,AOriImageStr
			, Size, AAutoIdentify, iResult)

	def ExtractImageFromURU4000(self, AOriImageBuf=defaultNamedNotOptArg, Size=defaultNamedNotOptArg, AAutoIdentify=defaultNamedNotOptArg, iResult=defaultNamedNotOptArg):
		return self._ApplyTypes_(69, 1, (3, 0), ((30, 1), (3, 1), (11, 1), (16396, 3)), 'ExtractImageFromURU4000', None,AOriImageBuf
			, Size, AAutoIdentify, iResult)

	def FlushFPImages(self):
		return self._oleobj_.InvokeTypes(19, LCID, 1, (24, 0), (),)

	def FreeFPCacheDB(self, fpcHandle=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((3, 1),),fpcHandle
			)

	def FreeFPCacheDBEx(self, fpcHandle=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(213, LCID, 1, (24, 0), ((3, 1),),fpcHandle
			)

	def GenRegTemplateAsStringFromFile(self, AImageFileName=defaultNamedNotOptArg, ADPI=defaultNamedNotOptArg, ADest=defaultNamedNotOptArg):
		return self._ApplyTypes_(205, 1, (11, 0), ((8, 1), (3, 1), (16392, 3)), 'GenRegTemplateAsStringFromFile', None,AImageFileName
			, ADPI, ADest)

	def GenVerTemplateAsStringFromFile(self, AImageFileName=defaultNamedNotOptArg, ADPI=defaultNamedNotOptArg, ADest=defaultNamedNotOptArg):
		return self._ApplyTypes_(206, 1, (11, 0), ((8, 1), (3, 1), (16392, 3)), 'GenVerTemplateAsStringFromFile', None,AImageFileName
			, ADPI, ADest)

	def GetFingerImage(self, AFingerImage=defaultNamedNotOptArg):
		return self._ApplyTypes_(46, 1, (11, 0), ((16396, 3),), 'GetFingerImage', None,AFingerImage
			)

	def GetTemplate(self):
		return self._ApplyTypes_(45, 1, (12, 0), (), 'GetTemplate', None,)

	def GetTemplateAsString(self):
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(39, LCID, 1, (8, 0), (),)

	def GetTemplateAsStringEx(self, AFPEngineVersion=defaultNamedNotOptArg):
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(219, LCID, 1, (8, 0), ((8, 1),),AFPEngineVersion
			)

	def GetTemplateCount(self, AFPHandle=defaultNamedNotOptArg, AOneToOneCnt=defaultNamedNotOptArg, ATotalCnt=defaultNamedNotOptArg):
		return self._ApplyTypes_(62, 1, (24, 0), ((3, 1), (16387, 3), (16387, 3)), 'GetTemplateCount', None,AFPHandle
			, AOneToOneCnt, ATotalCnt)

	def GetTemplateEx(self, AFPEngineVersion=defaultNamedNotOptArg):
		return self._ApplyTypes_(217, 1, (12, 0), ((8, 1),), 'GetTemplateEx', None,AFPEngineVersion
			)

	def GetVerScore(self):
		return self._oleobj_.InvokeTypes(60, LCID, 1, (3, 0), (),)

	def GetVerTemplate(self):
		return self._ApplyTypes_(59, 1, (12, 0), (), 'GetVerTemplate', None,)

	def GetVerTemplateEx(self, AFPEngineVersion=defaultNamedNotOptArg):
		return self._ApplyTypes_(220, 1, (12, 0), ((8, 1),), 'GetVerTemplateEx', None,AFPEngineVersion
			)

	def IdentificationFromFileInFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, pVerTemplateFile=defaultNamedNotOptArg, Score=defaultNamedNotOptArg, ProcessedFPNumber=defaultNamedNotOptArg):
		return self._ApplyTypes_(65, 1, (3, 0), ((3, 1), (8, 1), (16387, 3), (16387, 3)), 'IdentificationFromFileInFPCacheDB', None,fpcHandle
			, pVerTemplateFile, Score, ProcessedFPNumber)

	def IdentificationFromStrInFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, AVerTemplateStr=defaultNamedNotOptArg, Score=defaultNamedNotOptArg, ProcessedFPNumber=defaultNamedNotOptArg):
		return self._ApplyTypes_(66, 1, (3, 0), ((3, 1), (8, 1), (16387, 3), (16387, 3)), 'IdentificationFromStrInFPCacheDB', None,fpcHandle
			, AVerTemplateStr, Score, ProcessedFPNumber)

	def IdentificationInFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, pVerTemplate=defaultNamedNotOptArg, Score=defaultNamedNotOptArg, ProcessedFPNumber=defaultNamedNotOptArg):
		return self._ApplyTypes_(64, 1, (3, 0), ((3, 1), (12, 1), (16387, 3), (16387, 3)), 'IdentificationInFPCacheDB', None,fpcHandle
			, pVerTemplate, Score, ProcessedFPNumber)

	def InitEngine(self):
		return self._oleobj_.InvokeTypes(26, LCID, 1, (3, 0), (),)

	def IsOneToOneTemplate(self, ATemplate=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), ((12, 1),),ATemplate
			)

	def IsOneToOneTemplateFile(self, ATemplateFileName=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(34, LCID, 1, (11, 0), ((8, 1),),ATemplateFileName
			)

	def IsOneToOneTemplateStr(self, ATemplate=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(48, LCID, 1, (11, 0), ((8, 1),),ATemplate
			)

	def MF_GET_SNR(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, RDM_halt=defaultNamedNotOptArg
			, snr=defaultNamedNotOptArg, Value=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(221, LCID, 1, (11, 0), ((3, 1), (3, 1), (17, 1), (17, 1), (16401, 1), (16401, 1)),commHandle
			, DeviceAddress, mode, RDM_halt, snr, Value
			)

	def MF_GetSerNum(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, buffer=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(222, LCID, 1, (11, 0), ((3, 1), (3, 1), (16401, 1)),commHandle
			, DeviceAddress, buffer)

	def MF_GetVersionNum(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, VersionNum=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(224, LCID, 1, (11, 0), ((3, 1), (3, 1), (16401, 1)),commHandle
			, DeviceAddress, VersionNum)

	def MF_PCDRead(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, blkIndex=defaultNamedNotOptArg
			, blkNum=defaultNamedNotOptArg, key=defaultNamedNotOptArg, buffer=defaultNamedNotOptArg):
		return self._ApplyTypes_(225, 1, (11, 0), ((3, 1), (3, 1), (17, 1), (17, 1), (17, 1), (16401, 3), (16401, 3)), 'MF_PCDRead', None,commHandle
			, DeviceAddress, mode, blkIndex, blkNum, key
			, buffer)

	def MF_PCDWrite(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, blkIndex=defaultNamedNotOptArg
			, blkNum=defaultNamedNotOptArg, key=defaultNamedNotOptArg, buffer=defaultNamedNotOptArg):
		return self._ApplyTypes_(226, 1, (11, 0), ((3, 1), (3, 1), (17, 1), (17, 1), (17, 1), (16401, 3), (16401, 3)), 'MF_PCDWrite', None,commHandle
			, DeviceAddress, mode, blkIndex, blkNum, key
			, buffer)

	def MF_SetSerNum(self, commHandle=defaultNamedNotOptArg, DeviceAddress=defaultNamedNotOptArg, newValue=defaultNamedNotOptArg, buffer=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(223, LCID, 1, (11, 0), ((3, 1), (3, 1), (16401, 1), (16401, 1)),commHandle
			, DeviceAddress, newValue, buffer)

	def ModifyTemplate(self, ATemplate=defaultNamedNotOptArg, AOneToOne=defaultNamedNotOptArg):
		return self._ApplyTypes_(25, 1, (24, 0), ((16396, 3), (11, 1)), 'ModifyTemplate', None,ATemplate
			, AOneToOne)

	def ModifyTemplateFile(self, ATemplateFileName=defaultNamedNotOptArg, AOneToOne=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(58, LCID, 1, (24, 0), ((8, 1), (11, 1)),ATemplateFileName
			, AOneToOne)

	def ModifyTemplateStr(self, ATemplate=defaultNamedNotOptArg, AOneToOne=defaultNamedNotOptArg):
		return self._ApplyTypes_(57, 1, (24, 0), ((16392, 3), (11, 1)), 'ModifyTemplateStr', None,ATemplate
			, AOneToOne)

	def PrintImageAt(self, hdc=defaultNamedNotOptArg, x=defaultNamedNotOptArg, y=defaultNamedNotOptArg, aWidth=defaultNamedNotOptArg
			, aHeight=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(12, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),hdc
			, x, y, aWidth, aHeight)

	def PrintImageEllipseAt(self, hdc=defaultNamedNotOptArg, x=defaultNamedNotOptArg, y=defaultNamedNotOptArg, aWidth=defaultNamedNotOptArg
			, aHeight=defaultNamedNotOptArg, bkColor=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1), (19, 1)),hdc
			, x, y, aWidth, aHeight, bkColor
			)

	def RemoveRegTemplateFromFPCacheDB(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(16, LCID, 1, (3, 0), ((3, 1), (3, 1)),fpcHandle
			, FPID)

	def RemoveRegTemplateFromFPCacheDBEx(self, fpcHandle=defaultNamedNotOptArg, FPID=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(215, LCID, 1, (3, 0), ((3, 1), (3, 1)),fpcHandle
			, FPID)

	def SaveBitmap(self, FileName=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((8, 1),),FileName
			)

	def SaveJPG(self, FileName=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((8, 1),),FileName
			)

	def SaveTemplate(self, FileName=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(18, LCID, 1, (11, 0), ((8, 1), (12, 1)),FileName
			, ATemplate)

	def SaveTemplateStr(self, FileName=defaultNamedNotOptArg, ATemplateStr=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(61, LCID, 1, (24, 0), ((8, 1), (8, 1)),FileName
			, ATemplateStr)

	def SetAutoIdentifyPara(self, AAutoIdentify=defaultNamedNotOptArg, ACacheDBHandle=defaultNamedNotOptArg, AScore=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((11, 1), (3, 1), (3, 1)),AAutoIdentify
			, ACacheDBHandle, AScore)

	def SetImageDirection(self, AIsImageChange=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(41, LCID, 1, (24, 0), ((11, 1),),AIsImageChange
			)

	def SetOneToOnePara(self, ADoLearning=defaultNamedNotOptArg, AMatchSecurity=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(67, LCID, 1, (24, 0), ((3, 1), (3, 1)),ADoLearning
			, AMatchSecurity)

	def SetTemplateLen(self, ATemplate=defaultNamedNotOptArg, ALen=defaultNamedNotOptArg):
		return self._ApplyTypes_(208, 1, (3, 0), ((16396, 3), (3, 1)), 'SetTemplateLen', None,ATemplate
			, ALen)

	def UsingXTFTemplate(self, ADoUsingXTFTemplate=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(202, LCID, 1, (24, 0), ((11, 1),),ADoUsingXTFTemplate
			)

	def VerFinger(self, regTemplate=defaultNamedNotOptArg, verTemplate=defaultNamedNotOptArg, ADoLearning=defaultNamedNotOptArg, ARegFeatureChanged=defaultNamedNotOptArg):
		return self._ApplyTypes_(8, 1, (11, 0), ((16396, 3), (12, 1), (11, 1), (16395, 3)), 'VerFinger', None,regTemplate
			, verTemplate, ADoLearning, ARegFeatureChanged)

	def VerFingerFromFile(self, regTemplateFile=defaultNamedNotOptArg, verTemplateFile=defaultNamedNotOptArg, ADoLearning=defaultNamedNotOptArg, ARegFeatureChanged=defaultNamedNotOptArg):
		return self._ApplyTypes_(42, 1, (11, 0), ((8, 1), (8, 1), (11, 1), (16395, 3)), 'VerFingerFromFile', None,regTemplateFile
			, verTemplateFile, ADoLearning, ARegFeatureChanged)

	def VerFingerFromStr(self, regTemplateStr=defaultNamedNotOptArg, verTemplateStr=defaultNamedNotOptArg, ADoLearning=defaultNamedNotOptArg, ARegFeatureChanged=defaultNamedNotOptArg):
		return self._ApplyTypes_(36, 1, (11, 0), ((16392, 3), (8, 1), (11, 1), (16395, 3)), 'VerFingerFromStr', None,regTemplateStr
			, verTemplateStr, ADoLearning, ARegFeatureChanged)

	def VerRegFingerFile(self, regTemplateFile=defaultNamedNotOptArg, verTemplate=defaultNamedNotOptArg, ADoLearning=defaultNamedNotOptArg, ARegFeatureChanged=defaultNamedNotOptArg):
		return self._ApplyTypes_(10, 1, (11, 0), ((8, 1), (12, 1), (11, 1), (16395, 3)), 'VerRegFingerFile', None,regTemplateFile
			, verTemplate, ADoLearning, ARegFeatureChanged)

	_prop_map_get_ = {
		"Active": (32, 2, (11, 0), (), "Active", None),
		"EngineValid": (56, 2, (11, 0), (), "EngineValid", None),
		"EnrollCount": (6, 2, (3, 0), (), "EnrollCount", None),
		"EnrollIndex": (47, 2, (3, 0), (), "EnrollIndex", None),
		"FPEngineVersion": (51, 2, (8, 0), (), "FPEngineVersion", None),
		"ForceSecondMatch": (71, 2, (11, 0), (), "ForceSecondMatch", None),
		"ImageHeight": (53, 2, (3, 0), (), "ImageHeight", None),
		"ImageWidth": (52, 2, (3, 0), (), "ImageWidth", None),
		"IsRegister": (40, 2, (11, 0), (), "IsRegister", None),
		"IsReturnNoLic": (227, 2, (11, 0), (), "IsReturnNoLic", None),
		"IsSupportAuxDevice": (210, 2, (3, 0), (), "IsSupportAuxDevice", None),
		"LastQuality": (229, 2, (3, 0), (), "LastQuality", None),
		"LowestQuality": (230, 2, (3, 0), (), "LowestQuality", None),
		"OneToOneThreshold": (49, 2, (3, 0), (), "OneToOneThreshold", None),
		"ProduceName": (207, 2, (8, 0), (), "ProduceName", None),
		"RegTplFileName": (44, 2, (8, 0), (), "RegTplFileName", None),
		"ReservedParam": (228, 2, (3, 0), (), "ReservedParam", None),
		"SensorCount": (54, 2, (3, 0), (), "SensorCount", None),
		"SensorIndex": (37, 2, (3, 0), (), "SensorIndex", None),
		"SensorSN": (50, 2, (8, 0), (), "SensorSN", None),
		"TemplateLen": (55, 2, (3, 0), (), "TemplateLen", None),
		"Threshold": (21, 2, (3, 0), (), "Threshold", None),
		"Vendor": (203, 2, (8, 0), (), "Vendor", None),
		"VerTplFileName": (43, 2, (8, 0), (), "VerTplFileName", None),
	}
	_prop_map_put_ = {
		"Active": ((32, LCID, 4, 0),()),
		"EngineValid": ((56, LCID, 4, 0),()),
		"EnrollCount": ((6, LCID, 4, 0),()),
		"EnrollIndex": ((47, LCID, 4, 0),()),
		"FPEngineVersion": ((51, LCID, 4, 0),()),
		"ForceSecondMatch": ((71, LCID, 4, 0),()),
		"ImageHeight": ((53, LCID, 4, 0),()),
		"ImageWidth": ((52, LCID, 4, 0),()),
		"IsRegister": ((40, LCID, 4, 0),()),
		"IsReturnNoLic": ((227, LCID, 4, 0),()),
		"LowestQuality": ((230, LCID, 4, 0),()),
		"OneToOneThreshold": ((49, LCID, 4, 0),()),
		"RegTplFileName": ((44, LCID, 4, 0),()),
		"SensorCount": ((54, LCID, 4, 0),()),
		"SensorIndex": ((37, LCID, 4, 0),()),
		"SensorSN": ((50, LCID, 4, 0),()),
		"TemplateLen": ((55, LCID, 4, 0),()),
		"Threshold": ((21, LCID, 4, 0),()),
		"VerTplFileName": ((43, LCID, 4, 0),()),
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

class IZKFPEngXEvents:
	'Events interface for ZKFPEngX Control'
	CLSID = CLSID_Sink = IID('{8AEE2E53-7EBE-4B51-A964-009ADC68D107}')
	coclass_clsid = IID('{CA69969C-2F27-41D3-954D-A48B941C3BA7}')
	_public_methods_ = [] # For COM Server support
	_dispid_to_func_ = {
		        2 : "OnCaptureToFile",
		        5 : "OnFeatureInfo",
		        8 : "OnImageReceived",
		       10 : "OnCapture",
		       11 : "OnEnrollToFile",
		        3 : "OnFingerLeaving",
		        1 : "OnFingerTouching",
		        9 : "OnEnroll",
		}

	def __init__(self, oobj = None):
		if oobj is None:
			self._olecp = None
		else:
			import win32com.server.util
			from win32com.server.policy import EventHandlerPolicy
			cpc=oobj._oleobj_.QueryInterface(pythoncom.IID_IConnectionPointContainer)
			cp=cpc.FindConnectionPoint(self.CLSID_Sink)
			cookie=cp.Advise(win32com.server.util.wrap(self, usePolicy=EventHandlerPolicy))
			self._olecp,self._olecp_cookie = cp,cookie
	def __del__(self):
		try:
			self.close()
		except pythoncom.com_error:
			pass
	def close(self):
		if self._olecp is not None:
			cp,cookie,self._olecp,self._olecp_cookie = self._olecp,self._olecp_cookie,None,None
			cp.Unadvise(cookie)
	def _query_interface_(self, iid):
		import win32com.server.util
		if iid==self.CLSID_Sink: return win32com.server.util.wrap(self)

	# Event Handlers
	# If you create handlers, they should have the following prototypes:
#	def OnCaptureToFile(self, ActionResult=defaultNamedNotOptArg):
#	def OnFeatureInfo(self, AQuality=defaultNamedNotOptArg):
#	def OnImageReceived(self, AImageValid=defaultNamedNotOptArg):
#	def OnCapture(self, ActionResult=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):
#	def OnEnrollToFile(self, ActionResult=defaultNamedNotOptArg):
#	def OnFingerLeaving(self):
#	def OnFingerTouching(self):
#	def OnEnroll(self, ActionResult=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):


from win32com.client import CoClassBaseClass
# This CoClass is known by the name 'ZKFPEngXControl.ZKFPEngX'
class ZKFPEngX(CoClassBaseClass): # A CoClass
	# ZKFPEngX Control
	CLSID = IID('{CA69969C-2F27-41D3-954D-A48B941C3BA7}')
	coclass_sources = [
		IZKFPEngXEvents,
	]
	default_source = IZKFPEngXEvents
	coclass_interfaces = [
		IZKFPEngX,
	]
	default_interface = IZKFPEngX

IZKFPEngX_vtables_dispatch_ = 1
IZKFPEngX_vtables_ = [
	(( 'EnrollCount' , 'Value' , ), 6, (6, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( 'EnrollCount' , 'Value' , ), 6, (6, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( 'VerFinger' , 'regTemplate' , 'verTemplate' , 'ADoLearning' , 'ARegFeatureChanged' , 
			 'Value' , ), 8, (8, (), [ (16396, 3, None, None) , (12, 1, None, None) , (11, 1, None, None) , 
			 (16395, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( 'VerRegFingerFile' , 'regTemplateFile' , 'verTemplate' , 'ADoLearning' , 'ARegFeatureChanged' , 
			 'Value' , ), 10, (10, (), [ (8, 1, None, None) , (12, 1, None, None) , (11, 1, None, None) , 
			 (16395, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( 'PrintImageAt' , 'hdc' , 'x' , 'y' , 'aWidth' , 
			 'aHeight' , ), 12, (12, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( 'PrintImageEllipseAt' , 'hdc' , 'x' , 'y' , 'aWidth' , 
			 'aHeight' , 'bkColor' , ), 13, (13, (), [ (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( 'BeginEnroll' , ), 14, (14, (), [ ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( 'SaveTemplate' , 'FileName' , 'ATemplate' , 'Value' , ), 18, (18, (), [ 
			 (8, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( 'SaveBitmap' , 'FileName' , ), 22, (22, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( 'SaveJPG' , 'FileName' , ), 24, (24, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( 'InitEngine' , 'Value' , ), 26, (26, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( 'SensorIndex' , 'Value' , ), 37, (37, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( 'SensorIndex' , 'Value' , ), 37, (37, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( 'CancelEnroll' , ), 1, (1, (), [ ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( 'CreateFPCacheDB' , 'Value' , ), 9, (9, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( 'FreeFPCacheDB' , 'fpcHandle' , ), 11, (11, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateToFPCacheDB' , 'fpcHandle' , 'FPID' , 'pRegTemplate' , 'Value' , 
			 ), 15, (15, (), [ (3, 1, None, None) , (3, 1, None, None) , (12, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( 'RemoveRegTemplateFromFPCacheDB' , 'fpcHandle' , 'FPID' , 'Value' , ), 16, (16, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateFileToFPCacheDB' , 'fpcHandle' , 'FPID' , 'pRegTemplateFile' , 'Value' , 
			 ), 20, (20, (), [ (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( 'Threshold' , 'Value' , ), 21, (21, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( 'Threshold' , 'Value' , ), 21, (21, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( 'DongleIsExist' , 'Value' , ), 23, (23, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( 'DongleUserID' , 'Value' , ), 28, (28, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( 'DongleSeed' , 'lp2' , 'p1' , 'p2' , 'p3' , 
			 'p4' , 'Value' , ), 29, (29, (), [ (16387, 3, None, None) , (16387, 3, None, None) , 
			 (16387, 3, None, None) , (16387, 3, None, None) , (16387, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( 'DongleMemRead' , 'p1' , 'p2' , 'buf' , 'Value' , 
			 ), 30, (30, (), [ (16387, 3, None, None) , (16387, 3, None, None) , (16396, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( 'DongleMemWrite' , 'p1' , 'p2' , 'buf' , 'Value' , 
			 ), 38, (38, (), [ (16387, 3, None, None) , (16387, 3, None, None) , (16396, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( 'VerFingerFromFile' , 'regTemplateFile' , 'verTemplateFile' , 'ADoLearning' , 'ARegFeatureChanged' , 
			 'Value' , ), 42, (42, (), [ (8, 1, None, None) , (8, 1, None, None) , (11, 1, None, None) , 
			 (16395, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( 'VerTplFileName' , 'Value' , ), 43, (43, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( 'VerTplFileName' , 'Value' , ), 43, (43, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( 'RegTplFileName' , 'Value' , ), 44, (44, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( 'RegTplFileName' , 'Value' , ), 44, (44, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( 'GetTemplate' , 'Value' , ), 45, (45, (), [ (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( 'GetFingerImage' , 'AFingerImage' , 'Value' , ), 46, (46, (), [ (16396, 3, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( 'OneToOneThreshold' , 'Value' , ), 49, (49, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( 'OneToOneThreshold' , 'Value' , ), 49, (49, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( 'IsOneToOneTemplate' , 'ATemplate' , 'Value' , ), 7, (7, (), [ (12, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
	(( 'ModifyTemplate' , 'ATemplate' , 'AOneToOne' , ), 25, (25, (), [ (16396, 3, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( 'FlushFPImages' , ), 19, (19, (), [ ], 1 , 1 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
	(( 'Active' , 'Value' , ), 32, (32, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 180 , (3, 0, None, None) , 0 , )),
	(( 'Active' , 'Value' , ), 32, (32, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( 'IsRegister' , 'Value' , ), 40, (40, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 188 , (3, 0, None, None) , 0 , )),
	(( 'IsRegister' , 'Value' , ), 40, (40, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 192 , (3, 0, None, None) , 0 , )),
	(( 'EnrollIndex' , 'Value' , ), 47, (47, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 196 , (3, 0, None, None) , 0 , )),
	(( 'EnrollIndex' , 'Value' , ), 47, (47, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 200 , (3, 0, None, None) , 0 , )),
	(( 'SensorSN' , 'Value' , ), 50, (50, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 204 , (3, 0, None, None) , 0 , )),
	(( 'SensorSN' , 'Value' , ), 50, (50, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( 'FPEngineVersion' , 'Value' , ), 51, (51, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
	(( 'FPEngineVersion' , 'Value' , ), 51, (51, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 216 , (3, 0, None, None) , 0 , )),
	(( 'ImageWidth' , 'Value' , ), 52, (52, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 220 , (3, 0, None, None) , 0 , )),
	(( 'ImageWidth' , 'Value' , ), 52, (52, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 224 , (3, 0, None, None) , 0 , )),
	(( 'ImageHeight' , 'Value' , ), 53, (53, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 228 , (3, 0, None, None) , 0 , )),
	(( 'ImageHeight' , 'Value' , ), 53, (53, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 232 , (3, 0, None, None) , 0 , )),
	(( 'SensorCount' , 'Value' , ), 54, (54, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 236 , (3, 0, None, None) , 0 , )),
	(( 'SensorCount' , 'Value' , ), 54, (54, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 240 , (3, 0, None, None) , 0 , )),
	(( 'TemplateLen' , 'Value' , ), 55, (55, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 244 , (3, 0, None, None) , 0 , )),
	(( 'TemplateLen' , 'Value' , ), 55, (55, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 248 , (3, 0, None, None) , 0 , )),
	(( 'EngineValid' , 'Value' , ), 56, (56, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 252 , (3, 0, None, None) , 0 , )),
	(( 'EngineValid' , 'Value' , ), 56, (56, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 256 , (3, 0, None, None) , 0 , )),
	(( 'DecodeTemplate' , 'ASour' , 'ADest' , 'Value' , ), 2, (2, (), [ 
			 (8, 1, None, None) , (16396, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 260 , (3, 0, None, None) , 0 , )),
	(( 'EncodeTemplate' , 'ASour' , 'ADest' , 'Value' , ), 3, (3, (), [ 
			 (12, 1, None, None) , (16392, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 264 , (3, 0, None, None) , 0 , )),
	(( 'BeginCapture' , ), 4, (4, (), [ ], 1 , 1 , 4 , 0 , 268 , (3, 0, None, None) , 0 , )),
	(( 'CancelCapture' , ), 5, (5, (), [ ], 1 , 1 , 4 , 0 , 272 , (3, 0, None, None) , 0 , )),
	(( 'EndEngine' , ), 27, (27, (), [ ], 1 , 1 , 4 , 0 , 276 , (3, 0, None, None) , 0 , )),
	(( 'DecodeTemplate1' , 'ASour' , 'Value' , ), 31, (31, (), [ (8, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 280 , (3, 0, None, None) , 0 , )),
	(( 'EncodeTemplate1' , 'ASour' , 'Value' , ), 33, (33, (), [ (12, 1, None, None) , 
			 (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 284 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateStrToFPCacheDB' , 'fpcHandle' , 'FPID' , 'ARegTemplateStr' , 'Value' , 
			 ), 35, (35, (), [ (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 288 , (3, 0, None, None) , 0 , )),
	(( 'VerFingerFromStr' , 'regTemplateStr' , 'verTemplateStr' , 'ADoLearning' , 'ARegFeatureChanged' , 
			 'Value' , ), 36, (36, (), [ (16392, 3, None, None) , (8, 1, None, None) , (11, 1, None, None) , 
			 (16395, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 292 , (3, 0, None, None) , 0 , )),
	(( 'GetTemplateAsString' , 'Value' , ), 39, (39, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 296 , (3, 0, None, None) , 0 , )),
	(( 'IsOneToOneTemplateStr' , 'ATemplate' , 'Value' , ), 48, (48, (), [ (8, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 300 , (3, 0, None, None) , 0 , )),
	(( 'ModifyTemplateStr' , 'ATemplate' , 'AOneToOne' , ), 57, (57, (), [ (16392, 3, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 304 , (3, 0, None, None) , 0 , )),
	(( 'SaveTemplateStr' , 'FileName' , 'ATemplateStr' , ), 61, (61, (), [ (8, 1, None, None) , 
			 (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 308 , (3, 0, None, None) , 0 , )),
	(( 'GetTemplateCount' , 'AFPHandle' , 'AOneToOneCnt' , 'ATotalCnt' , ), 62, (62, (), [ 
			 (3, 1, None, None) , (16387, 3, None, None) , (16387, 3, None, None) , ], 1 , 1 , 4 , 0 , 312 , (3, 0, None, None) , 0 , )),
	(( 'IdentificationInFPCacheDB' , 'fpcHandle' , 'pVerTemplate' , 'Score' , 'ProcessedFPNumber' , 
			 'Value' , ), 64, (64, (), [ (3, 1, None, None) , (12, 1, None, None) , (16387, 3, None, None) , 
			 (16387, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 316 , (3, 0, None, None) , 0 , )),
	(( 'IdentificationFromFileInFPCacheDB' , 'fpcHandle' , 'pVerTemplateFile' , 'Score' , 'ProcessedFPNumber' , 
			 'Value' , ), 65, (65, (), [ (3, 1, None, None) , (8, 1, None, None) , (16387, 3, None, None) , 
			 (16387, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 320 , (3, 0, None, None) , 0 , )),
	(( 'IdentificationFromStrInFPCacheDB' , 'fpcHandle' , 'AVerTemplateStr' , 'Score' , 'ProcessedFPNumber' , 
			 'Value' , ), 66, (66, (), [ (3, 1, None, None) , (8, 1, None, None) , (16387, 3, None, None) , 
			 (16387, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 324 , (3, 0, None, None) , 0 , )),
	(( 'SetAutoIdentifyPara' , 'AAutoIdentify' , 'ACacheDBHandle' , 'AScore' , ), 17, (17, (), [ 
			 (11, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 328 , (3, 0, None, None) , 0 , )),
	(( 'SetImageDirection' , 'AIsImageChange' , ), 41, (41, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 332 , (3, 0, None, None) , 0 , )),
	(( 'IsOneToOneTemplateFile' , 'ATemplateFileName' , 'Value' , ), 34, (34, (), [ (8, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 336 , (3, 0, None, None) , 0 , )),
	(( 'ModifyTemplateFile' , 'ATemplateFileName' , 'AOneToOne' , ), 58, (58, (), [ (8, 1, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 340 , (3, 0, None, None) , 0 , )),
	(( 'GetVerTemplate' , 'Value' , ), 59, (59, (), [ (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 344 , (3, 0, None, None) , 0 , )),
	(( 'GetVerScore' , 'Value' , ), 60, (60, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 348 , (3, 0, None, None) , 0 , )),
	(( 'AddImageFile' , 'AFileName' , 'ADPI' , 'Value' , ), 63, (63, (), [ 
			 (8, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 352 , (3, 0, None, None) , 0 , )),
	(( 'SetOneToOnePara' , 'ADoLearning' , 'AMatchSecurity' , ), 67, (67, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 356 , (3, 0, None, None) , 0 , )),
	(( 'CompressTemplate' , 'ATemplate' , 'Value' , ), 68, (68, (), [ (8, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 360 , (3, 0, None, None) , 0 , )),
	(( 'ConvertAttTemplate' , 'ATemplate' , 'Value' , ), 70, (70, (), [ (12, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 364 , (3, 0, None, None) , 0 , )),
	(( 'ForceSecondMatch' , 'Value' , ), 71, (71, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 368 , (3, 0, None, None) , 0 , )),
	(( 'ForceSecondMatch' , 'Value' , ), 71, (71, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 372 , (3, 0, None, None) , 0 , )),
	(( 'AddBitmap' , 'BitmapHandle' , 'ValidRectX1' , 'ValidRectY1' , 'ValidRectX2' , 
			 'ValidRectY2' , 'DPI' , 'Value' , ), 201, (201, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 376 , (3, 0, None, None) , 0 , )),
	(( 'UsingXTFTemplate' , 'ADoUsingXTFTemplate' , ), 202, (202, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 380 , (3, 0, None, None) , 0 , )),
	(( 'ExtractImageFromURU4000' , 'AOriImageBuf' , 'Size' , 'AAutoIdentify' , 'iResult' , 
			 'Value' , ), 69, (69, (), [ (30, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , 
			 (16396, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 384 , (3, 0, None, None) , 0 , )),
	(( 'ConvertToBiokey' , 'OriTemplate' , 'NewTemlate' , ), 204, (204, (), [ (12, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 388 , (3, 0, None, None) , 0 , )),
	(( 'GenRegTemplateAsStringFromFile' , 'AImageFileName' , 'ADPI' , 'ADest' , 'Value' , 
			 ), 205, (205, (), [ (8, 1, None, None) , (3, 1, None, None) , (16392, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 392 , (3, 0, None, None) , 0 , )),
	(( 'GenVerTemplateAsStringFromFile' , 'AImageFileName' , 'ADPI' , 'ADest' , 'Value' , 
			 ), 206, (206, (), [ (8, 1, None, None) , (3, 1, None, None) , (16392, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 396 , (3, 0, None, None) , 0 , )),
	(( 'ExtractImageFromURU' , 'AOriImageStr' , 'Size' , 'AAutoIdentify' , 'iResult' , 
			 'Value' , ), 691, (691, (), [ (8, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , 
			 (16396, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 400 , (3, 0, None, None) , 0 , )),
	(( 'Vendor' , 'Value' , ), 203, (203, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 404 , (3, 0, None, None) , 0 , )),
	(( 'ProduceName' , 'Value' , ), 207, (207, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 408 , (3, 0, None, None) , 0 , )),
	(( 'SetTemplateLen' , 'ATemplate' , 'ALen' , 'Value' , ), 208, (208, (), [ 
			 (16396, 3, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 412 , (3, 0, None, None) , 0 , )),
	(( 'ControlSensor' , 'ACode' , 'AValue' , 'Value' , ), 209, (209, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 416 , (3, 0, None, None) , 0 , )),
	(( 'IsSupportAuxDevice' , 'Value' , ), 210, (210, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 420 , (3, 0, None, None) , 0 , )),
	(( 'ExtractImageFromTerminal' , 'AOriImage' , 'Size' , 'AAutoIdentify' , 'iResult' , 
			 'Value' , ), 211, (211, (), [ (12, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , 
			 (16396, 3, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 424 , (3, 0, None, None) , 0 , )),
	(( 'CreateFPCacheDBEx' , 'Value' , ), 212, (212, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 428 , (3, 0, None, None) , 0 , )),
	(( 'FreeFPCacheDBEx' , 'fpcHandle' , ), 213, (213, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 432 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateToFPCacheDBEx' , 'fpcHandle' , 'FPID' , 'pRegTemplate' , 'pRegTemplate10' , 
			 'Value' , ), 214, (214, (), [ (3, 1, None, None) , (3, 1, None, None) , (12, 1, None, None) , 
			 (12, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 436 , (3, 0, None, None) , 0 , )),
	(( 'RemoveRegTemplateFromFPCacheDBEx' , 'fpcHandle' , 'FPID' , 'Value' , ), 215, (215, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 440 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateFileToFPCacheDBEx' , 'fpcHandle' , 'FPID' , 'pRegTemplateFile' , 'pRegTemplate10File' , 
			 'Value' , ), 216, (216, (), [ (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , 
			 (8, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 444 , (3, 0, None, None) , 0 , )),
	(( 'GetTemplateEx' , 'AFPEngineVersion' , 'Value' , ), 217, (217, (), [ (8, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 448 , (3, 0, None, None) , 0 , )),
	(( 'AddRegTemplateStrToFPCacheDBEx' , 'fpcHandle' , 'FPID' , 'ARegTemplateStr' , 'ARegTemplate10Str' , 
			 'Value' , ), 218, (218, (), [ (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , 
			 (8, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 452 , (3, 0, None, None) , 0 , )),
	(( 'GetTemplateAsStringEx' , 'AFPEngineVersion' , 'Value' , ), 219, (219, (), [ (8, 1, None, None) , 
			 (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 456 , (3, 0, None, None) , 0 , )),
	(( 'GetVerTemplateEx' , 'AFPEngineVersion' , 'Value' , ), 220, (220, (), [ (8, 1, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 460 , (3, 0, None, None) , 0 , )),
	(( 'MF_GET_SNR' , 'commHandle' , 'DeviceAddress' , 'mode' , 'RDM_halt' , 
			 'snr' , 'Value' , 'pVal' , ), 221, (221, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (16401, 1, None, None) , (16401, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 464 , (3, 0, None, None) , 0 , )),
	(( 'MF_GetSerNum' , 'commHandle' , 'DeviceAddress' , 'buffer' , 'pVal' , 
			 ), 222, (222, (), [ (3, 1, None, None) , (3, 1, None, None) , (16401, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 468 , (3, 0, None, None) , 0 , )),
	(( 'MF_SetSerNum' , 'commHandle' , 'DeviceAddress' , 'newValue' , 'buffer' , 
			 'pVal' , ), 223, (223, (), [ (3, 1, None, None) , (3, 1, None, None) , (16401, 1, None, None) , 
			 (16401, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 472 , (3, 0, None, None) , 0 , )),
	(( 'MF_GetVersionNum' , 'commHandle' , 'DeviceAddress' , 'VersionNum' , 'pVal' , 
			 ), 224, (224, (), [ (3, 1, None, None) , (3, 1, None, None) , (16401, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 476 , (3, 0, None, None) , 0 , )),
	(( 'MF_PCDRead' , 'commHandle' , 'DeviceAddress' , 'mode' , 'blkIndex' , 
			 'blkNum' , 'key' , 'buffer' , 'pVal' , ), 225, (225, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (16401, 3, None, None) , (16401, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 480 , (3, 0, None, None) , 0 , )),
	(( 'MF_PCDWrite' , 'commHandle' , 'DeviceAddress' , 'mode' , 'blkIndex' , 
			 'blkNum' , 'key' , 'buffer' , 'pVal' , ), 226, (226, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (16401, 3, None, None) , (16401, 3, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 484 , (3, 0, None, None) , 0 , )),
	(( 'ReservedParam' , 'Value' , ), 228, (228, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 488 , (3, 0, None, None) , 0 , )),
	(( 'IsReturnNoLic' , 'Value' , ), 227, (227, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 492 , (3, 0, None, None) , 0 , )),
	(( 'IsReturnNoLic' , 'Value' , ), 227, (227, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 496 , (3, 0, None, None) , 0 , )),
	(( 'LastQuality' , 'Value' , ), 229, (229, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 500 , (3, 0, None, None) , 0 , )),
	(( 'LowestQuality' , 'Value' , ), 230, (230, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 504 , (3, 0, None, None) , 0 , )),
	(( 'LowestQuality' , 'Value' , ), 230, (230, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 508 , (3, 0, None, None) , 0 , )),
]

RecordMap = {
}

CLSIDToClassMap = {
	'{CA69969C-2F27-41D3-954D-A48B941C3BA7}' : ZKFPEngX,
	'{161A8D2D-3DDE-4744-BA38-08F900D10D6D}' : IZKFPEngX,
	'{8AEE2E53-7EBE-4B51-A964-009ADC68D107}' : IZKFPEngXEvents,
}
CLSIDToPackageMap = {}
win32com.client.CLSIDToClass.RegisterCLSIDsFromDict( CLSIDToClassMap )
VTablesToPackageMap = {}
VTablesToClassMap = {
	'{161A8D2D-3DDE-4744-BA38-08F900D10D6D}' : 'IZKFPEngX',
}


NamesToIIDMap = {
	'IZKFPEngXEvents' : '{8AEE2E53-7EBE-4B51-A964-009ADC68D107}',
	'IZKFPEngX' : '{161A8D2D-3DDE-4744-BA38-08F900D10D6D}',
}


